module Class (Class(..), Move(..), Effect(..), Stat(..), Result(..)) where

data Class = Class {
  className    :: String,
  classTrigger :: String,
  startingMoves :: [Move],
  advancedMoves :: [Move]
}

header :: String
header = "\\documentclass{article}\n\\usepackage{tcolorbox}\n\\usepackage[margin=.5in]{geometry}\n\\begin{document}\n"

footer :: String
footer = "\n\\end{document}\n"

tcolorbox :: String -> String -> String
tcolorbox title contents = "\\begin{tcolorbox}[title=" ++ title ++ "]\n" ++ contents ++ "\n\\end{tcolorbox}\n"


instance Show Class where
  show (Class name trigger sMoves aMoves) = header ++ "\\section*{" ++ name ++ "}\n" ++ tcolorbox "Trigger" ("When you \\textbf{" ++ trigger ++ "}, you may take this class.") ++ tcolorbox "Starting Moves" (concatMap show sMoves) ++ tcolorbox "Advanced Moves" (concatMap show aMoves) ++ footer

data Move = Move {
  moveName :: String,
  moveEffect :: Effect
}

instance Show Move where
  show (Move name effect) = "\\subsection*{" ++ name ++ "}\n" ++ show effect ++ "\n"

data Result = Choose Integer | Do String

instance Show Result where
  show (Choose n) = "Choose " ++ show n ++ " from the list below"
  show (Do s) = s

data Effect =
  TextEffect String |
  TriggerEffect String Result [String] |
  RollEffect String Stat Result Result (Maybe Result) [String]

instance Show Effect where
  show (TextEffect contents) = contents
  show (TriggerEffect trigger contents []) =
    "When you \\textbf{" ++ trigger ++ "}" ++ show contents
  show (TriggerEffect trigger contents options @ (_:_)) =
    "When you \\textbf{" ++ trigger ++ "}" ++ show contents ++ "\\begin{itemize}\n" ++ concatMap ("\n\\item "++) options
  show (RollEffect trigger stat success partial Nothing options) =
    show $ TriggerEffect trigger (Do $ "On a 10+ " ++ show success ++ ". On a 7--9 " ++ show partial ++ "." ) options
  show (RollEffect trigger stat success partial (Just failure) options) =
    show ( RollEffect trigger stat success partial Nothing options ) ++ "On a miss " ++ show failure

data Stat = WIS | INT | CHA | STR | DEX | CON deriving Show
  

cook :: Class
cook = Class "Cook" "prepare a meal that amazes those who eat it" [
  Move "Grocery Shopping" $ TriggerEffect "are in a settlement and search for gourmet ingredients" (Do "the GM will give you a price.  You may gain \\textit{supply} by paying that much for each supply you gain.") []] []

main = print cook
